import web
import view, config
from view import render
import emotion

urls = (
    '/', 'index',
    '/question','question'
)

class index:
    def GET(self):
        return render.base(view.listing())

class question:
    form = web.form.Form(
        web.form.Textarea('question', web.form.notnull, 
            rows=5, cols=20,
            description="Enter text to detect its emotion:"),
        web.form.Button('Judge me'),
    )
    
    def GET(self):
    	form = self.form()
    	# form_text = render.base(form)
        return render.base(form = form,title = "Are you an optimist?")
    def POST(self):
        form = self.form()
        if not form.validates():
            
            return render.base(form = form,title="Are you an optimist?")
        feedback = emotion.emotion(form.d.question)
        return render.base(form = form,title="Are you an optimist: ",p1=feedback[0],p2=feedback[1])

if __name__ == "__main__":
    app = web.application(urls, globals())
    app.internalerror = web.debugerror
    app.run()