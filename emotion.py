from text.blob import TextBlob

def emotion(text):

	r = TextBlob(text)
	return r.sentiment

if __name__=='__main__':
	print emotion("the crowd can be red with anger")